import { screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { render } from "../../test-utils";
import HomePage from "../index";

const clickRequestInviteButton = () => {
  userEvent.click(
    screen.getByRole("button", {
      name: "Request an invite",
    })
  );
};

const closeModal = () => {
  userEvent.click(
    screen.getByRole("button", {
      name: "Cancel",
    })
  );
};

describe("<HomePage />", () => {
  it("can render without error", async () => {
    render(<HomePage />);
    expect(screen.findByText("Brocolli & Co.")).toBeTruthy();
  });

  it("can render invite modal", async () => {
    render(<HomePage />);
    clickRequestInviteButton();
    await waitFor(async () => {
      expect(await screen.findByRole("dialog")).toBeTruthy();
    });

    closeModal();

    await waitFor(
      async () => {
        expect(await (await screen.queryAllByRole("dialog")).length).toBe(0);
      },
      { timeout: 2000 } // Modal animation timeout
    );
  });
});
