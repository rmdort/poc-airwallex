import {
  Heading,
  Box,
  Text,
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";
import type { NextPage } from "next";
import Head from "next/head";
import React, { useState } from "react";
import { Body } from "./components/Body";
import { Footer } from "./components/Footer";
import { Header } from "./components/Header";
import { InviteForm } from "./components/InviteForm";

const Home: NextPage = () => {
  const [isInviteModalOpen, setInviteModalOpen] = useState(false);
  const openModal = () => setInviteModalOpen(true);
  const closeModal = () => setInviteModalOpen(false);
  return (
    <>
      <Head>
        <title>Brocolli & Co.</title>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
      </Head>
      <Header />

      <Body>
        <Box
          flex={1}
          justifyContent="center"
          alignItems="center"
          display="flex"
          flexDirection="column"
        >
          <Heading
            as="h1"
            fontSize={{
              lg: "xxx-large",
              base: "x-large",
            }}
            mb={6}
            textAlign="center"
          >
            A better way <br />
            to enjoy every day.
          </Heading>
          <Text fontSize="lg" mb={4}>
            Be the first to know when we launch
          </Text>
          <Button
            variant="outline"
            colorScheme="teal"
            size="lg"
            onClick={openModal}
            mb={6}
          >
            Request an invite
          </Button>
        </Box>

        <Modal onClose={closeModal} isOpen={isInviteModalOpen} isCentered>
          <ModalOverlay />
          <ModalContent>
            <ModalCloseButton />
            <ModalBody>
              <InviteForm onCancel={closeModal} />
            </ModalBody>
          </ModalContent>
        </Modal>
      </Body>

      <Footer />
    </>
  );
};

export default Home;
