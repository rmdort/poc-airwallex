import theme, { Theme } from "@chakra-ui/theme";

const customTheme: Theme = {
  ...theme,
  styles: {
    ...theme.styles,
    global: {
      "html, body, #__next, #app": {
        minHeight: "100vh",
      },
      "#__next": {
        display: "flex",
        flexDirection: "column",
      },
    },
  },
};

export default customTheme;
