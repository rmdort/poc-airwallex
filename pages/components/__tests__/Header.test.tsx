import { screen } from "@testing-library/react";
import useEvent from "@testing-library/user-event";
import { render, routerMock } from "../../../test-utils";
import { Header } from "../Header";
import { RouterContext } from "next/dist/shared/lib/router-context";
import { m } from "framer-motion";

describe("<Header />", () => {
  it("can render without error", () => {
    render(<Header />);
    expect(screen.getByText("Brocolli & Co.")).toBeTruthy();
  });

  it("can route to homepage", () => {
    const pushFn = jest.fn();
    render(
      <RouterContext.Provider
        value={{
          ...routerMock,
          push: pushFn,
        }}
      >
        <Header />
      </RouterContext.Provider>
    );
    useEvent.click(screen.getByTestId("logo"));
    expect(pushFn).toHaveBeenCalledWith("/", "/", {
      locale: undefined,
      scroll: undefined,
      shallow: undefined,
    });
  });
});
