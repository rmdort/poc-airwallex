import { screen, waitFor } from "@testing-library/react";
import { render } from "../../../test-utils";
import { InviteForm } from "../InviteForm";
import fetch from "jest-fetch-mock";
import userEvent from "@testing-library/user-event";

const defaultName = "foo";
const defaultEmail = "foo@foobar.com";
const duplicateEmail = "usedemail@airwallex.com";
const enterName = (value: string = defaultName) => {
  userEvent.type(
    screen.getByRole("textbox", {
      name: "Full name",
    }),
    value
  );
};

const enterEmail = (value: string = defaultEmail) => {
  userEvent.type(
    screen.getByRole("textbox", {
      name: "Email",
    }),
    value
  );
};

const enterConfirmEmail = (value: string = defaultEmail) => {
  userEvent.type(
    screen.getByRole("textbox", {
      name: "Confirm email",
    }),
    value
  );
};

const submitForm = () =>
  userEvent.click(
    screen.getByRole("button", {
      name: "Send",
    })
  );

describe("<InviteForm />", () => {
  beforeEach(() => {
    fetch.resetMocks();
  });
  it("can render without error", () => {
    render(<InviteForm />);
    expect(screen.getByText("Request an invite")).toBeTruthy();
  });

  it("can submit the form", async () => {
    fetch.mockResponseOnce(JSON.stringify("Registered"));
    render(<InviteForm />);
    enterName();
    enterEmail();
    enterConfirmEmail();
    submitForm();

    await waitFor(async () => {
      expect(await screen.findByText("All done!")).toBeTruthy();
    });
  });

  it("can do client side validations", async () => {
    fetch.mockResponseOnce(JSON.stringify("Registered"));
    render(<InviteForm />);
    submitForm();

    await waitFor(async () => {
      expect(await screen.findByText("name is a required field")).toBeTruthy();
    });
  });

  it("can handle null response exceptions", async () => {
    fetch.mockResponseOnce(JSON.stringify(null));
    render(<InviteForm />);
    enterName();
    enterEmail();
    enterConfirmEmail();
    submitForm();

    await waitFor(async () => {
      expect(await screen.findByText("Something went wrong")).toBeTruthy();
    });
  });

  it("can do server side validation of duplicate emails", async () => {
    const errorMessage = "Bad Request: Email is already in use";
    const response = new Response(JSON.stringify({ errorMessage }), {
      status: 400,
    });
    fetch.mockResolvedValue(response);
    render(<InviteForm />);
    enterName();
    enterEmail(duplicateEmail);
    enterConfirmEmail(duplicateEmail);
    submitForm();

    await waitFor(async () => {
      expect(await screen.findByText(errorMessage)).toBeTruthy();
    });
  });
});
