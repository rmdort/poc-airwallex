import { screen } from "@testing-library/react";
import { render } from "../../../test-utils";
import { Footer } from "../Footer";
describe("<Footer />", () => {
  it("can render without error", () => {
    render(<Footer />);
    expect(screen.getByText("Made with ❤ in Melbourne.")).toBeTruthy();
  });
});
