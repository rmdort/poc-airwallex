import { screen } from "@testing-library/react";
import { render } from "../../../test-utils";
import { Body } from "../Body";
describe("<Body />", () => {
  it("can render without error", () => {
    render(<Body>child</Body>);
    expect(screen.getByText("child")).toBeTruthy();
  });
});
