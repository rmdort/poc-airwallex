import { Box, Divider } from "@chakra-ui/layout";
import { Button, Heading, Text } from "@chakra-ui/react";
import { Form, Formik, FormikConfig } from "formik";
import { InputControl, ResetButton, SubmitButton } from "formik-chakra-ui";
import React, { FC, useCallback, useState } from "react";
import { object, ref, string } from "yup";
import { BASE_URL } from "../config";

const validationSchema = object().shape({
  name: string().required(),
  email: string().email().required(),
  emailConfirmation: string()
    .oneOf([ref("email")], "Emails must match")
    .required(),
});

const GENERIC_ERROR = "Something went wrong";

type InviteModel = {
  name: string;
  email: string;
  emailConfirmation: string;
};

type Props = {
  onCancel?: () => void;
};

export const InviteForm: FC<Props> = ({ onCancel }) => {
  const [formError, setFormError] = useState<string | null>(null);
  const [isRegistered, setIsRegistered] = useState(false);
  const handleSubmit = useCallback<FormikConfig<InviteModel>["onSubmit"]>(
    async (values, formikHelper) => {
      formikHelper.setSubmitting(true);
      setFormError(null);
      try {
        const response = await fetch(BASE_URL, {
          method: "POST",
          body: JSON.stringify({
            name: values.name,
            email: values.email,
          }),
        })
          .then(async (res) => {
            if (!res.ok) {
              const error = await res.json();
              throw new Error(error.errorMessage);
            }
            return res;
          })
          .then((res) => res.json());

        if (response) {
          setIsRegistered(true);
        } else {
          throw GENERIC_ERROR;
        }
      } catch (err) {
        if (err instanceof Error) {
          setFormError(err.message);
        } else {
          setFormError(GENERIC_ERROR);
        }
      } finally {
        formikHelper.setSubmitting(false);
      }
    },
    []
  );
  return (
    <>
      <Heading pt={6} pb={6} as="h2" fontSize="x-large">
        {isRegistered ? "All done!" : "Request an invite"}
      </Heading>
      <Divider mb={6} />
      {isRegistered ? (
        <>
          <Text mb={6}>
            You will be one of the first to experience Brocolli &amp; Co. when
            we launch
          </Text>
          <Button
            type="button"
            variant="outline"
            onClick={onCancel}
            isFullWidth
            mb={6}
          >
            Ok
          </Button>
        </>
      ) : (
        <Formik<InviteModel>
          initialValues={{
            name: "",
            email: "",
            emailConfirmation: "",
          }}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          <Form>
            <InputControl mb={6} isRequired label="Full name" name="name" />
            <InputControl mb={6} isRequired label="Email" name="email" />
            <InputControl
              mb={6}
              isRequired
              label="Confirm email"
              name="emailConfirmation"
            />
            <Box
              display="grid"
              gridTemplateColumns="repeat(2, 1fr)"
              gridGap={4}
              mb={6}
            >
              <SubmitButton isFullWidth>Send</SubmitButton>
              <Button type="button" variant="outline" onClick={onCancel}>
                Cancel
              </Button>
            </Box>
            {formError && (
              <Box color="red.400" fontSize="sm" mb={6}>
                {formError}
              </Box>
            )}
          </Form>
        </Formik>
      )}
    </>
  );
};
