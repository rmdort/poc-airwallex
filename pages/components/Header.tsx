import { Container, Divider } from "@chakra-ui/layout";
import React from "react";
import Link from "next/link";
import { Link as ChakraLink } from "@chakra-ui/react";

export const Header = () => {
  return (
    <>
      <Container as="header" maxW="container.xl" pt={6} pb={6}>
        <Link href="/" passHref>
          <ChakraLink
            textTransform="uppercase"
            fontWeight="bold"
            data-testid="logo"
          >
            Brocolli & Co.
          </ChakraLink>
        </Link>
      </Container>
      <Divider />
    </>
  );
};
