import { Box, Container } from "@chakra-ui/react";
import React, { FC } from "react";

export const Body: FC = ({ children }) => {
  return (
    <Box as="main" flex={1} overflow="auto" bg="gray.100" display="flex">
      <Container
        maxW="container.xl"
        pt={6}
        pb={6}
        flex={1}
        display="flex"
        flexDirection="row"
      >
        {children}
      </Container>
    </Box>
  );
};
