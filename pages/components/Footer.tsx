import { Container, Divider } from "@chakra-ui/layout";
import React from "react";
import { Text } from "@chakra-ui/react";

export const Footer = () => {
  return (
    <>
      <Divider />
      <Container
        as="footer"
        maxW="container.xl"
        pt={6}
        pb={6}
        textAlign="center"
      >
        <Text fontStyle="italic">Made with ❤ in Melbourne.</Text>
        <Text fontStyle="italic">
          &copy; {new Date().getFullYear()} Brocolli &amp; Co. All rights
          reserved
        </Text>
      </Container>
    </>
  );
};
