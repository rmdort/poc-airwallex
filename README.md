This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

1. Clone the repository

2. Install node modules

```
npm i
```

3. Run development server

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

4. Run tests

```bash
npm run test
```

5. Linting

```bash
npm run lint
```

6. Formatting (Prettier)

```bash
npm run format
```
