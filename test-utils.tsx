import { ChakraProvider } from "@chakra-ui/react";
import { render as defaultRender } from "@testing-library/react";
import React from "react";
import customTheme from "./pages/theme";

export const render = (ui: React.ReactElement) =>
  defaultRender(<ChakraProvider theme={customTheme}>{ui}</ChakraProvider>);

export const routerMock = {
  route: "/",
  pathname: "/",
  asPath: "/",
  query: {},
  basePath: "/",
  isLocaleDomain: false,
  replace: () => Promise.resolve(true),
  prefetch: () => Promise.resolve(),
  reload: jest.fn(),
  back: jest.fn(),
  beforePopState: jest.fn(),
  events: {
    on: jest.fn(),
    off: jest.fn(),
    emit: jest.fn(),
  },
  isFallback: false,
  isReady: false,
  isPreview: false,
};
